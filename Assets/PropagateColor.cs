﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PropagateColor : MonoBehaviour
{
    #region Public Properties

    public float Emission
    {
        set
        {
            for (int i = 0; i < _children.Count; i++)
            {
                _children[ i ].Emission = value;
            }
        }
    }

    public Color Albedo
    {
        set
        {
            for (int i = 0; i < _children.Count; i++)
            {
                _children[ i ].Albedo = value;
            }
        }
    }

    public Color EmissionColor
    {
        set
        {
            for (int i = 0; i < _children.Count; i++)
            {
                _children[ i ].EmissionColor = value;
            }
        }
    }


    #endregion

    private List<MaterialControl> _children = new List<MaterialControl>();

    void Awake()
    {
        for ( int i = 0; i < transform.childCount; i++ )
        {
            var t = transform.GetChild( i );

            if ( t.gameObject.GetComponent<MaterialControl>() == null )
            {
                t.gameObject.AddComponent<MaterialControl>();
            }

            // setup
            var matControl = t.GetComponent<MaterialControl>();

            _children.Add( matControl );
        }
    }

}
