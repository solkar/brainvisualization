﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class NumericPanel : MonoBehaviour 
{
    [SerializeField] InputField _field;
    [SerializeField] Text       _title;
    [SerializeField] Slider     _slider;

    public int    max;
    public string title;
    public int inputValue;

    public UnityEvent onValueChange;

    void Start()
    {
        // update placeholders to max
        Text placeholder = (Text) _field.placeholder;
        placeholder.text =  "0... " + max.ToString();

        // update slider max
        _slider.maxValue = max;

        // set title
        _title.text = title;

        // set init value
        int initValue = (int)(max * 0.2f);
        _field.text = initValue.ToString();
        _slider.value = initValue;

        // cap max value
        _field.onEndEdit.AddListener( delegate{ OnEndEdit( _field );});

        _slider.onValueChanged.AddListener( delegate{ OnValueChange( _slider ); });
    }

    void OnEndEdit( InputField input )
    {
        if (input.text.Length > 0) 
		{
			//Debug.Log("Text has been entered");
            inputValue = int.Parse( input.text );

            if( inputValue > max )
            {
                inputValue = max;
                input.text = max.ToString();
            }

            if( onValueChange != null )
                onValueChange.Invoke();

            SyncValues( inputValue );
		}
    }

    void OnValueChange( Slider slider )
    {
        inputValue = (int) slider.value;

        if( onValueChange != null )
            onValueChange.Invoke();

        SyncValues( inputValue );
    }

    void SyncValues( int newValue )
    {
        _slider.value = newValue;
        _field.text = newValue.ToString();
    }
}
