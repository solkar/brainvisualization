﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SparkController : MonoBehaviour 
{
    [SerializeField] Material _sparkMaterial;

    private List<AnimateEmission> _children = new List<AnimateEmission>();

#region Public Properties

    public float Speed
    {
        set
        {
            int activeChildren = Mathf.FloorToInt( _active * _children.Count );
            
            for (int i = 0; i < _children.Count; i++) 
            {
                if( i < activeChildren ) 
                {
                    _children[ i ].Speed = 1; 
                    _children[ i ].OffsetSpeed = new Vector2( 0 , value );
                }
                else
                {
                    _children[ i ].Speed = 0;
                }
            }
        }
    }

    public float Burst
    {
        set
        {
            for (int i = 0; i < _children.Count; i++) 
            {
               _children[ i ].Burst = value; 
            }
        }
    }

    public float Active
    {
        set{ _active = value;  }

    }
    private float _active;

    public float Emission
    {
        set
        {
            for (int i = 0; i < _children.Count; i++) 
            {
               _children[ i ].Emission = value; 
            }
        }
    }
#endregion

#region MonoBehaviour Functions

    void Awake()
    {

//		Debug.Log (name + " children:" + transform.childCount);

        // get all children
        for( int i = 0; i < transform.childCount; i++ )
        {
            var t = transform.GetChild( i );
            
            // add AnimateEmission component
            if( t.gameObject.GetComponent<AnimateEmission>() == null )
            {
                t.gameObject.AddComponent<AnimateEmission>();
            }
            
            // setup
            var sparkComponent = t.GetComponent<AnimateEmission>();
            sparkComponent.GetComponent<Renderer>().material = _sparkMaterial;
            sparkComponent.OffsetSpeed = new Vector2( 0f , 0.2f );

			
            // store into a list
            _children.Add( sparkComponent );
        }
    }

#endregion

}
