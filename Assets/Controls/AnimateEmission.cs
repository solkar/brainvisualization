﻿using UnityEngine;
using System.Collections;

public class AnimateEmission : MonoBehaviour 
{

    [SerializeField] float _speedX;
    [SerializeField] float _speedY;

    public float Speed
    {
        set{ _offsetY = value; }
    }
    private float _offsetY = 1f;

    public float Burst
    {
        set{ _scale = new Vector2( 1 , value ); }
    }
    private Vector2 _scale = new Vector2( 1f , 1f );

    public float Emission
    {
        set{ _emission = value; }
    }
    private float _emission;

    public Vector2 OffsetSpeed
    {
        set
        {
            _speedX = value.x;
            _speedY = value.y;
        }
    }

    private Color _emissionColor;

    void Start()
    {
        Burst = 1;
        Speed = 0;

        _emissionColor = GetComponent<Renderer>().material.GetColor( "_EmissionColor" );
    }

    void Update()
    {
        float offsetX = Mathf.Repeat( _speedX * Time.time , 1 );
        float offsetY = Mathf.Repeat( _speedY * _offsetY * Time.time , 1 );

        var offset = new Vector2( offsetX * _scale.x , offsetY * _scale.y );
        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", offset );
        GetComponent<Renderer>().material.SetTextureScale("_MainTex",  _scale );
        GetComponent<Renderer>().material.SetColor("_EmissionColor",  _emissionColor * _emission );
    }
    
}
