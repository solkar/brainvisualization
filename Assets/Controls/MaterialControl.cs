﻿using UnityEngine;
using System.Collections;
using Klak.MaterialExtension;

public class MaterialControl : MonoBehaviour 
{

    public float Emission
    {
        set{ _emission = value; }
    }
    private float _emission;

    public Color Albedo
    {
        set{ _albedoColor = value; }
    }
    private Color _albedoColor;

    public Color EmissionColor
    {
        set{ _emissionColor = value; }
    }
    private Color _emissionColor;

    MaterialPropertyBlock _materialProperties;

    [SerializeField] bool _useAlbedoColorForEmission = true;


    void Start()
    {
        _materialProperties = new MaterialPropertyBlock();

        gameObject.GetComponent<Renderer>().material.SetInt("_ZWrite", 0);
        ChangeMaterial();
    }

    void Update()
    {

        ChangeMaterial();
        GetComponent<Renderer>().SetPropertyBlock(_materialProperties);
    }
    
    void ChangeMaterial()
    {
        Color updateEmission;

        if( _useAlbedoColorForEmission )
        {
            updateEmission = _albedoColor * _emission;
        }
        else
        {
            updateEmission = _emissionColor * _emission;
        }

        _materialProperties
            .Property("_EmissionColor", updateEmission )
            .Property( "_Color" , _albedoColor );
    }
}
