﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
//using Klak;

public class NumericControlPanel : MonoBehaviour 
{
    #region Nested Public Classes 

    [Serializable] public class ValueEvent : UnityEvent<float> {}

    #endregion

    #region Editable Properties 

    [HeaderAttribute( "Titles")]
    [SerializeField] string _coreTitle;
    [SerializeField] string _cortexTitle;
    [SerializeField] string _synapsisTitle;

    [SerializeField] int _coreMax;
    [SerializeField] int _cortexMax;
    [SerializeField] int _synapsisMax;

    [SerializeField] NumericPanel _core;
    [SerializeField] NumericPanel _cortex;
    [SerializeField] NumericPanel _synapsis;

    #endregion

    public float coreRatio;
    public float cortexRatio;
    public float synapsisRatio;

    [SerializeField] ValueEvent _coreValueEvent;
    [SerializeField] ValueEvent _cortexValueEvent;
    [SerializeField] ValueEvent _synapsisValueEvent;

    void Awake()
    {
        _core.max     = _coreMax;
        _cortex.max   = _cortexMax;
        _synapsis.max = _synapsisMax;

        _core.title     = _coreTitle;
        _cortex.title   = _cortexTitle;
        _synapsis.title = _synapsisTitle;

        _core.onValueChange.AddListener( delegate{ OnValueChange( _core , out coreRatio ); } );
        _cortex.onValueChange.AddListener( delegate{ OnValueChange( _cortex , out cortexRatio ); } );
        _synapsis.onValueChange.AddListener( delegate{ OnValueChange( _synapsis , out synapsisRatio ); } );
    }

    void OnValueChange( NumericPanel panel , out float ratio )
    {
       ratio = ( (float) panel.inputValue ) / panel.max ;
    }

    void Update()
    {
        _coreValueEvent.Invoke( coreRatio );
        _cortexValueEvent.Invoke( cortexRatio );
        _synapsisValueEvent.Invoke( synapsisRatio );
    }

}
