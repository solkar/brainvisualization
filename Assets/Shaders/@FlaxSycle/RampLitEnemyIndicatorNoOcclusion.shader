﻿Shader "Ramp Lit Enemy Indicator No Occlusion"
{
    Properties 
    {
        _Color ("Main Color", Color) = (1,1,1,1)
        _MainTex ("Base (RGB)", 2D) = "grey" {}
        _SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
        _Shininess ("Shininess", Range (0.01, 1)) = 0.078125
        _IndicatorTex ("Indicator Lights (RGB)", 2D) = "black" {}
        _Indicator ("Indicator Color", Color) = (1,1,1,1)
    }
     
    SubShader 
    {
        Tags { "Queue" = "Geometry" "RenderType"="Opaque" }
   
		CGPROGRAM
		#pragma surface surf BlinnPhong
   
        uniform float4 _Color;
        uniform float4 _Indicator;
        uniform half _Shininess;
        uniform sampler2D _MainTex;
        uniform sampler2D _IndicatorTex;
       
        struct Input {
            float2 uv_MainTex;
            float3 viewDir;
        };
       
        void surf (Input IN, inout SurfaceOutput o) {
            o.Albedo = tex2D ( _MainTex, IN.uv_MainTex).rgb * _Color;
            o.Emission = tex2D ( _IndicatorTex, IN.uv_MainTex).rgb * _Indicator * 3;
           
            half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
            o.Emission += tex2D ( _MainTex, IN.uv_MainTex).rgb * 1.0 * pow (rim, 2.5);
           
            o.Gloss = 1.0 - tex2D ( _IndicatorTex, IN.uv_MainTex).r;
            o.Specular = _Shininess;
        }
   
		ENDCG
    }
   
    Fallback "VertexLit"
}