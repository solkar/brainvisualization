﻿using UnityEngine;
using System.Collections;

public class LoadMaterialIntoChildren : MonoBehaviour 
{
    [SerializeField] Material _material;

    void Awake()
    {
        for( int i = 0; i < transform.childCount; i++ )
        {
            var t = transform.GetChild( i );
            
            t.GetComponent<Renderer>().material = _material;
        }
    }
}
