﻿using UnityEngine;
using System.Collections;

public class DelayActivate : MonoBehaviour
{
		public float Delay;
		public GameObject toEnable;

		// Use this for initialization
		IEnumerator Start ()
		{
				yield return new WaitForSeconds (Delay);
				toEnable.SetActive (true);
		}
}
