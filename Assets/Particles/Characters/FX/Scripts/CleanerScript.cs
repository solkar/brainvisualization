﻿using UnityEngine;
using System.Collections;

public class CleanerScript : MonoBehaviour
{
		public float destructionTime;

		// Use this for initialization
		void Start ()
		{
				if (destructionTime > 0)
						Destroy (gameObject, destructionTime);
		}
	
		// Update is called once per frame
		/*void Update ()
		{
		}*/
}
