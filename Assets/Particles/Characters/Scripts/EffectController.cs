﻿//
//
// EffectController  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;

public class EffectController : MonoBehaviour 
{
    public GameObject effect;

    public void Stop()
    {
        effect.SetActive( false );
    }

}
