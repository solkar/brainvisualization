﻿using UnityEngine;
using System.Collections;

public class QuitApplication : MonoBehaviour 
{
	void Update () {
		if( Input.GetKeyDown( KeyCode.Escape ) )
		{

			#if UNITY_STANDALONE
			Application.Quit();
			#endif

			#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
			#endif

		}

	}
}
